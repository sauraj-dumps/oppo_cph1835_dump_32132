## full_oppo6771-user 10 QP1A.190711.020 7604ad4e2b1328a1 release-keys
- Manufacturer: oppo
- Platform: mt6771
- Codename: CPH1835
- Brand: OPPO
- Flavor: full_oppo6771-user
- Release Version: 10
- Id: QP1A.190711.020
- Incremental: 7604ad4e2b1328a1
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: 
- OTA version: CPH1819EX_11.F.27_2270_202104090928
- Branch: CPH1819EX_11.F.27_2270_202104090928
- Repo: oppo_cph1835_dump_32132


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
